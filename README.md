CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Provides a DIBS payment gateway integration.

This is a very simple module which allows you to send your clients to make an
off-site payment directly on the DIBS payment system.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_dibs

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/commerce_dibs


REQUIREMENTS
------------

This module requires following outside of Drupal core:

 * Drupal Commerce - https://www.drupal.org/project/commerce
 * Registration with DIBS - https://www.dibspayment.com/card_payments


INSTALLATION
------------

 * Install the Commerce DIBS integration module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Commerce > Configuration > Payment gateways
       to add one of DIBS payment gateways.
        - In case of PayWin or FlexWin - enter the Merchant ID number, it is the DIBS Customer ID.
        - In case of Easy - enter Secret key - You can find the secretKey and checkoutKey for the test and live environment in the Easy administration
    4. Fill the fields and save it.
    5. You will now be able to select DIBS payment during the checkout.


MAINTAINERS
-----------

 * casaran - https://www.drupal.org/u/casaran
 * Andreas Albers (AndreasAlbers) - https://www.drupal.org/u/andreasalbers
 * zaporylie - https://www.drupal.org/u/zaporylie

Supporting organizations:

 * Linkfactory A/S - https://www.drupal.org/linkfactory-as
 * Ny Media AS - https://www.drupal.org/ny-media-as
