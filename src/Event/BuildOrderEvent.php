<?php

namespace Drupal\commerce_dibs\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\EventDispatcher\Event;

class BuildOrderEvent extends Event {

  /**
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * @var array
   */
  protected $dibsOrder;

  /**
   * BuildOrderEvent constructor.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param array $dibs_order
   */
  public function __construct(OrderInterface $order, array $dibs_order) {
    $this->order = $order;
    $this->dibsOrder = $dibs_order;
  }

  /**
   * @param array $dibsOrder
   *
   * @return $this
   */
  public function setDibsOrder($dibsOrder) {
    $this->dibsOrder = $dibsOrder;
    return $this;
  }

  /**
   * @return array
   */
  public function getDibsOrder() {
    return $this->dibsOrder;
  }

  /**
   * @return \Drupal\commerce_order\Entity\OrderInterface
   */
  public function getOrder() {
    return $this->order;
  }

}
